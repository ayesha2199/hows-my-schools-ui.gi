import './style.scss'
import { Modal, Button, Tooltip} from 'carbon-components-react'
import React, { useState } from 'react'
import ModalTableComponent from '../ModalTable/modalTable.js'

export const ModalComponent = ({ toggleModal, show, data, onSelectSchoolById}) => {
  const [rows, setRows] = useState([]);

  return (
    show && (
      <Modal
        open
        onRequestClose={() => toggleModal()}
        passiveModal
        className="modal"
      >
        <div className="modal-heading">
          {" "}
          Search Ontario Schools
          <Tooltip direction="top" tabIndex={0}>
            <p>This table shows data for all Ontario schools. Search for any school and add it to view more data.</p>
          </Tooltip>
        </div>
        <div>
          <ModalTableComponent
            data={data || []}
            setRows={(row) => setRows(row)}
            prevRow={rows}
            onSelectSchoolById={onSelectSchoolById}
          />
        </div>
        <div style={{ display: "flex", justifyContent: "flex-end" }}>
          <Button onClick={() => toggleModal()}>Add +</Button>
        </div>
      </Modal>
    )
  );
};

export default ModalComponent;
