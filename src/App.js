import './App.scss'
import React, { useState } from 'react'
import Header from './Header/header.js'
import {BrowserRouter, Route, Redirect, Switch} from 'react-router-dom'
import TableComponent from './Table/table.js'
import {TransitionGroup, CSSTransition} from 'react-transition-group'
import EmailSignUp from './Email/email.js'
import MainPage from './Pages/MainPage'
import AllSchools from './Pages/AllSchools'
import GraphComponent from './Graph/graph.js'
import Papa from 'papaparse'

function App() {


    return (
      <BrowserRouter>
      <Route render={({ location }) => {
          
        return(
        <div className='absolute-position'>
           <Header />
          <TransitionGroup component="div" className="App">
            <CSSTransition timeout={300} classNames='page' key={location.pathname}> 
              <Switch location={location}>
                    <Route path='/' exact render={() => {
                      return(
                       <MainPage/>
                      )}
                    }/>
                    <Route path='/schools'  exact render={() => {
                      return(
                        <AllSchools/>
                      )
                    }

                    }/>
              </Switch>
            </CSSTransition>
          </TransitionGroup>
        </div>
      )}} />
      </BrowserRouter>
    );
  }

export default App;
