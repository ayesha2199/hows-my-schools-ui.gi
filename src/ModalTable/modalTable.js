import "./style.scss";
import {
  DataTable,
  Loading,
  TableSelectRow,
  Pagination,
  TableSelectAll,
  TableContainer,
  TableToolbarSearch,
  Table,
  TableHead,
  TableToolbar,
  TableToolbarContent,
  TableHeader,
  TableBody,
  TableCell,
  TableRow,
} from "carbon-components-react";
import { getCurrentPageRows } from "../util/lib";
import React, { useState, useEffect } from "react";

const headerData = [
  {
    header: "School Name",
    key: "school",
  },
  {
    header: "School Board",
    key: "school_board",
  },
];

const ModalTableComponent = ({ data, prevRow, onSelectSchoolById }) => {
  const [paginationSettings, setPaginationSettings] = useState({
    page: 1,
    pageSize: 5,
  });

  const getPaginationProps = () => {
    return {
      page: paginationSettings.page,
      pageSize: paginationSettings.pageSize,
      onChange: handlePaginationChange,
    };
  };

  const handlePaginationChange = (e) => {
    setPaginationSettings({
      page: e.page,
      pageSize: e.pageSize,
    });
  };

  const updateRows = (rows) => {
    return getCurrentPageRows(rows, data, paginationSettings);
  };

  const renderProps = {
    getCurrentPageRows: getCurrentPageRows,
    getPaginationProps: getPaginationProps,
  };

  return (
    <div className="modal-table-container">
      {data.length === 0 ? (
        <Loading description="Active loading indicator" withOverlay={false} />
      ) : (
        <DataTable rows={data} headers={headerData}>
          {({
            rows,
            headers,
            selectedRows,
            getHeaderProps,
            getRowProps,
            getSelectionProps,
            getTableProps,
            getToolbarProps,
            onInputChange,
          }) => (
            <TableContainer>
              <TableToolbar {...getToolbarProps()}>
                <TableToolbarContent>
                  <TableToolbarSearch onChange={onInputChange} />
                </TableToolbarContent>
              </TableToolbar>
              <Table {...getTableProps()}>
                <TableHead>
                  <TableRow>
                    {/* TODO: Implement Select All */}
                    <th></th>
                    {/* <TableSelectAll
                      onChange={
                        selectedRows.length !== 0 &&
                        prevRow.length !== selectedRows.length
                          ? setRows(selectedRows)
                          : null
                      }
                    /> */}
                    {headers.map((header) => (
                      <TableHeader {...getHeaderProps({ header })}>
                        {header.header}
                      </TableHeader>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {updateRows(rows).map((row) => (
                    <TableRow key={row.id}>
                      <TableSelectRow
                        className="select-button"
                        {...getSelectionProps({ row })}
                        onChange={(selected) => {
                          onSelectSchoolById(row.school_id, selected);
                        }}
                        checked={row.selected}
                      />
                      {row.cells.map((cell) => (
                        <TableCell key={cell.id}>{cell.value}</TableCell>
                      ))}
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
              <Pagination
                {...renderProps.getPaginationProps()}
                pageSizes={[5, 10, 20, 30, 50, 60, 75]}
                itemsPerPageText={"Items per page"}
                backwardText={"Prev"}
                forwardText={"Next"}
                totalItems={rows.length}
              />
            </TableContainer>
          )}
        </DataTable>
      )}
    </div>
  );
};

export default ModalTableComponent;
