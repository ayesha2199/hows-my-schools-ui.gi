import "./style.scss";
import { Button, Tooltip, Dropdown, TextInput } from "carbon-components-react";
import { useState } from "react";

const EmailSignUp = ({ schoolsData }) => {
  let selectedSchools = schoolsData.filter((school) => school.display);
  console.log('[DEBUG][EmailSignUp] selected schools:', selectedSchools);
  const [email, setEmail] = useState("");
  const [frequency, setFrequency] = useState("");

  const handleSignUp = () => {
    let body = {};
    // TODO: Show error when no schools selected
    if (selectedSchools && selectedSchools.length) {
      body = {
        email: email,
        frequency: frequency.label.toLowerCase(),
        school_ids: selectedSchools.map((school) => school.school_id),
      };

      console.log(JSON.stringify(body));

      fetch("https://hmf-schools.herokuapp.com/api/schools/mail", {
        method: "post",
        mode: "cors",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      }).then((response) => {
        console.log(response);
        console.error();
      });
    }
  };

  return (
    <div className="email-signup-container">
      <div className="email-heading">
        {" "}
        Sign-up for Email
        <Tooltip direction="top" tabIndex={0}>
          <p>
            Sign up to receive email updates. Please provide an email address, select the schools you're interested in and select email frequency.
          </p>
        </Tooltip>
      </div>
      <div className="dropdown">
        <Dropdown
          id="inline"
          titleText="Frequency:"
          label="Select frequency"
          type="inline"
          onChange={({ selectedItem }) => setFrequency(selectedItem)}
          items={items}
        />
        <div className="text-input-container">
          <div className="box-title">Email:</div>
          <TextInput
            id="test2"
            onChange={(e) => setEmail(e.target.value)}
            className="text-input"
            invalidText="A valid value is required"
            placeholder="Enter your email here"
            labelText=""
          />
        </div>
        <div className="text-input-container">
          <div className="school-title">Selected schools:</div>
          <div className="school-name-container" key="school-name-container">
            {selectedSchools && selectedSchools.length
              ? selectedSchools.map((school) => (
                  <div key={`${school.school_id}-email`} className="school">{school.school}</div>
                ))
              : null}
          </div>
        </div>
      </div>
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <Button className="signup-button" onClick={() => handleSignUp()}>
          Sign up
        </Button>
      </div>
    </div>
  );
};

const items = [
  {
    id: "option-1",
    label: "Daily",
  },
  {
    id: "option-2",
    label: "Weekly",
  }
];

export default EmailSignUp;
