import React, { useState, useEffect } from 'react'
import {withRouter} from 'react-router-dom';
import {Tooltip} from 'carbon-components-react'
import AllSchoolsTable from '../AllSchoolsTable/AllSchoolsTable.js'
import Papa from 'papaparse'
import { useHistory } from "react-router";
import './style.scss'

const AllSchools = () => {
  const history = useHistory()

  const [items, setItems] = useState([])
  const [dataIsLoaded, setDataIsLoaded] = useState(false)

  // Fetch data from schools endpoint - csv file converted to array
  useEffect(() => {
      const fetchData = () => {
          fetch(
              "https://hmf-schools.herokuapp.com/data/schoolsrecentcovid?unique=true")
              .then((response) => response.text())
              .then ((data) => Papa.parse(data))
              .then((data)=> {
                  setItems(data)
                  setDataIsLoaded(true)
                  console.log("CSV data loaded.")
              })
      }

      if (!dataIsLoaded) {
          fetchData();
      }
  }, []);
  return (
    <div className="App">
        <header className="App-header">
            <div className="tableContainer">
                <div className="table-title">Covid-19 Status in Ontario Schools
                        <Tooltip direction="top" tabIndex={0}>
                            <p>This table shows COVID-19 data for all Ontario schools. Search for any school in the search bar.</p>
                        </Tooltip>
                    </div>
                <AllSchoolsTable data={items} />
            </div>
        </header>
    </div>
  );
}

export default withRouter(AllSchools);