import "../App.scss";
import React, { useState, useEffect } from "react";
import TableComponent from "../Table/table.js";
import EmailSignUp from "../Email/email.js";
import GraphComponent from "../Graph/graph.js";

function MainPage() {
  const [schoolsData, setSchoolsData] = useState([]);
  const [graphData, setGraphData] = useState([]);
  const [dataIsLoaded, setDataIsLoaded] = useState(false);

  // Fetch data from schools endpoint
  useEffect(() => {
    const fetchData = () => {
      // Get cached data
      let cached_data = localStorage.getItem("cached_data");
      if (cached_data !== null) {
        console.log("[DEBUG][localStorage] Using cached_data");
        setSchoolsData(JSON.parse(cached_data));
        setDataIsLoaded(true);
        return;
      }

      // Fetch table data
      fetch(
        "https://hmf-schools.herokuapp.com/api/schoolsrecentcovid?unique=true&as_list=true"
      )
        .then((response) => response.json())
        .then((data) => {
          // TODO: Add risk level
          let formattedData = data.map((obj) => ({
            ...obj,
            selected: false,
            display: false, // TODO: Consider changing field name
            risk: obj.total_confirmed_cases
          }));

          localStorage.setItem("cached_data", JSON.stringify(formattedData));

          setSchoolsData(formattedData);
          setDataIsLoaded(true);
          console.log(
            "[LOG] Data loaded. Number items: ",
            formattedData.length
          );
        });
    };

    const fetchGraphData = async () => {
      // Get cached data
      let cached_graph_data = localStorage.getItem("cached_graph_data");
      if (cached_graph_data !== null) {
        console.log("[DEBUG][localStorage] Using cached_graph_data");
        setGraphData(JSON.parse(cached_graph_data));
        return;
      }

      // Fetch graph data
      const url = "https://hmf-schools.herokuapp.com/api/schoolsrecentcovid";
      const response = await fetch(url);
      const data = JSON.parse(await response.text());
      setGraphData(data);
      localStorage.setItem("cached_graph_data", JSON.stringify(data));
    };

    if (!dataIsLoaded) {
      fetchData();
      fetchGraphData();
    }
  });

  const selectSchoolById = (schoolId, selected) => {
    let updatedSchoolsData = schoolsData.map((school) =>
      school.school_id === schoolId ? {
        ...school,
        selected: selected,
        ...(selected === false && { display: false })
      } : school
    );
    setSchoolsData(updatedSchoolsData);
  };

  const selectAllSchools = (selected) => {
    let updatedSchoolsData = schoolsData.map((school) => ({
      ...school,
      selected: selected,
    }));
    setSchoolsData(updatedSchoolsData);
  };

  const displaySchoolById = (schoolId, display) => {
    let updatedSchoolsData = schoolsData.map((school) =>
      school.school_id === schoolId ? { ...school, display: display } : school
    );
    setSchoolsData(updatedSchoolsData);
  };

  const displayAllSchools = (display) => {
    let updatedSchoolsData = schoolsData.map((school) => ({
      ...school,
      display: display,
    }));
    setSchoolsData(updatedSchoolsData);
  };

  return (
    <div className="App">
      <header className="App-header">
        <div className="tableContainer">
          <TableComponent
            key="table_key"
            data={schoolsData}
            selectSchoolById={selectSchoolById}
            selectAllSchools={selectAllSchools}
            displaySchoolById={displaySchoolById}
            displayAllSchools={displayAllSchools}
          />
        </div>
        <div className="container-two">
          <div className="emailContainer">
            <EmailSignUp key="email_key" schoolsData={schoolsData} />
          </div>
          <div className="graphContainer">
            <GraphComponent
              key="graph_key"
              schoolsData={schoolsData}
              graphData={graphData}
            />
          </div>
        </div>
      </header>
    </div>
  );
}

export default MainPage;
