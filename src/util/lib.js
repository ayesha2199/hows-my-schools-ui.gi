export const getCurrentPageRows = (rows, data, paginationSettings) => {
    let lastItemIndex = 0;
    let { page, pageSize } = paginationSettings;

    if (page === 1 || rows.length <= pageSize) {
        lastItemIndex = pageSize;
        page = 1;
    } else {
        lastItemIndex = pageSize * page;
    }

    let subRows = rows.slice((page - 1) * pageSize, lastItemIndex);

    let combinedData = []
    subRows.forEach((row) => {
        let schoolName = row.cells[0].value
        let schoolData = data.filter((school) => schoolName === school.school)[0];
        let combinedSchool = {...row, ...schoolData };
        combinedData.push(combinedSchool);
    })

    return combinedData;
};