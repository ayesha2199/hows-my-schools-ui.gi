import "./style.scss";
import {
  DataTable,
  Button,
  Pagination,
  TableSelectRow,
  Tooltip,
  TableSelectAll,
  TableContainer,
  TableToolbarSearch,
  Table,
  TableHead,
  TableToolbar,
  TableToolbarContent,
  TableHeader,
  TableBody,
  TableCell,
  TableRow,
} from "carbon-components-react";
import ModalComponent from "../Modal/modal.js";
import EmptyComponent from "../EmptyState/emptyState";
import { getCurrentPageRows } from "../util/lib";
import React, { useState } from "react";
import { CheckmarkFilled24, WarningAltFilled24} from '@carbon/icons-react';

const headerData = [
  {
    header: "School Name",
    key: "school",
  },
  {
    header: "School Board",
    key: "school_board",
  },
  {
    header: "Total Confirmed Cases",
    key: "total_confirmed_cases",
  },
  {
    header: "Risk Level",
    key: "risk",
  },
];

const TableComponent = ({
  data,
  selectSchoolById,
  selectAllSchools,
  displaySchoolById,
  displayAllSchools,
}) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [paginationSettings, setPaginationSettings] = useState({
    page: 1,
    pageSize: 5,
  });

  const toggleModal = () => {
    setIsModalOpen(!isModalOpen);
  };

  const getPaginationProps = () => {
    return {
      page: paginationSettings.page,
      pageSize: paginationSettings.pageSize,
      onChange: handlePaginationChange,
    };
  };

  const handlePaginationChange = (e) => {
    setPaginationSettings({
      ...paginationSettings,
      page: e.page,
      pageSize: e.pageSize,
    });
  };

  const getRisk = (cases) => {
      if (cases < 1) return <><CheckmarkFilled24 style={{fill: 'green'}}/></>
      if (cases >= 1 && cases <5) return <><WarningAltFilled24 style={{fill: '#f0e51a'}}/></>
      if (cases >=5 && cases <9) return <><WarningAltFilled24 style={{fill: 'orange'}}/></>
      if (cases >=9) return <><WarningAltFilled24 style={{fill: 'red'}}/></>
  }

  return (
    <div className="table-container">
      <div className="table-title">
        Covid-19 Status in Ontario Schools
        <Tooltip direction="top" tabIndex={0}>
          <p>Some info here</p>
        </Tooltip>
      </div>
      <DataTable rows={data.filter((row) => row.selected)} headers={headerData}>
        {({
          rows,
          headers,
          selectedRows,
          getHeaderProps,
          getSelectionProps,
          getTableProps,
          getToolbarProps,
          onInputChange,
        }) => (
          <TableContainer>
            <TableToolbar {...getToolbarProps()}>
              <TableToolbarContent>
                <TableToolbarSearch onChange={onInputChange} />
                <Button
                  onClick={(e) => setIsModalOpen(true)}
                  size="small"
                  kind="primary"
                >
                  Add your school +
                </Button>
              </TableToolbarContent>
            </TableToolbar>

            <Table {...getTableProps()}>
              <TableHead>
                <TableRow>
                  {/* TODO: Implement Select All */}
                  <th></th>
                  {/* <TableSelectAll
                    {...getSelectionProps()}
                    id="select-all-table"
                    name="select-all-table"
                    onSelect={(selected) => {
                      const toggleSelectAll = !selectAllChecked;
                      setSelectAllChecked(toggleSelectAll);
                      displayAllSchools(toggleSelectAll);
                    }}
                    checked={selectAllChecked}
                  /> */}
                  {headers.map((header) => (
                    <TableHeader {...getHeaderProps({ header })}>
                      {header.header}
                    </TableHeader>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {getCurrentPageRows(rows, data, paginationSettings).map(
                  (row) => (
                    <TableRow key={row.id}>
                      <TableSelectRow
                        className="select-button"
                        {...getSelectionProps({ row })}
                        checked={row.display}
                        onSelect={(selected) => {
                          displaySchoolById(row.school_id, selected);
                        }}
                      />
                      {row.cells.map((cell) => (
                        (cell.info.header === "risk") ? (
                          <TableCell key={cell.id}>{getRisk(cell.value)}</TableCell>
                        ) : (
                          <TableCell key={cell.id}>{cell.value}</TableCell>
                        )
                      ))}
                      {/* TODO: Allow option to remove school */}
                    </TableRow>
                  )
                )}
              </TableBody>
            </Table>
            {rows && rows.length > 0 ? (
              <Pagination
                {...getPaginationProps()}
                pageSizes={[5, 10, 20, 30, 50, 60, 75]}
                itemsPerPageText={"Items per page"}
                backwardText={"Prev"}
                forwardText={"Next"}
                totalItems={rows.length}
              />
            ) : (
              <EmptyComponent />
            )}
          </TableContainer>
        )}
      </DataTable>
      <ModalComponent
        toggleModal={toggleModal}
        show={isModalOpen}
        data={data}
        onSelectSchoolById={selectSchoolById}
      />
    </div>
  );
};

export default TableComponent;
