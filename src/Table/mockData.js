export const headerData = [
    {
      header: 'School Name',
      key: 'name',
    },
    {
      header: 'District',
      key: 'district',
    },
    {
      header: 'Cases',
      key: 'cases',
    },
    {
      header: 'Risk Level',
      key: 'risk',
    },
  ];

  export const rowData = [
    {
      name: 'School A Secondary School',
      district: 'Toronto District School Board',
      cases: '4',
      risk: 3,
    },
    {
        name: 'School B Elementary School',
        district: 'Toronto District School Board',
        cases: '12',
        risk: 3,
    },
    {
        name: 'Central Peel Secondary School',
        district: 'Toronto Catholic District School Board',
        cases: '8',
        risk: 4,
    },
    {
        name: 'Some Random School',
        district: 'Some Random District',
        cases: '2',
        risk: 5,
    },
  ];