import "./style.scss";
import React, { useState } from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend
} from "recharts";

const GraphComponent = ({ schoolsData, graphData }) => {
  // TODO: Allow user to set the dates
  const [startDate] = useState("2021-09-14");
  const [endDate] = useState("2021-09-23");
  let selectedSchools = schoolsData.filter((school) => school.display);
  let filteredGraphData = graphData.filter(
    (school) =>
      school.reported_date >= startDate && school.reported_date <= endDate
  );

  const colours = [
    "#FF0000",
    "#008000",
    "#0000FF",
    "#ff7f50",
    "#9932cc",
    "#ff00ff",
  ];

  return (
    <div className="graph-container">
      <div className="graph-heading">Covid-19 Trend in Schools</div>
      {selectedSchools.length && filteredGraphData.length ? (
        <LineChart width={650} height={450} data={filteredGraphData}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="reported_date" />
          <YAxis />
          <Tooltip />
          <Legend />
          {selectedSchools.map((school, index) => (
            <Line
              type="monotone"
              dataKey={school.school_id}
              name={school.school}
              stroke={colours[index]}
            />
          ))}
        </LineChart>
      ) : (
        <></>
      )}
      {/* TODO: Not sure if this loading state is needed anymore or if the logic I set needs to be changed */}
      {/* <Loading description="Active loading indicator" withOverlay={false} /> */}
    </div>
  );
};

export default GraphComponent;
