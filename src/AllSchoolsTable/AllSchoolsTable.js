
import './style.scss'
import { DataTable, Loading, TableSelectRow, Pagination, TableSelectAll, TableContainer, TableToolbarSearch, Table, TableHead, TableToolbar, TableToolbarContent, TableHeader, TableBody, TableCell, TableRow} from 'carbon-components-react'
import React from 'react';
import { Add24, CheckmarkFilled24, WarningAltFilled24} from '@carbon/icons-react';



const getRows = (data) => {
    const schoolRows = data.data
    let rows = []
    if (schoolRows && schoolRows.length !== 0){
        schoolRows.forEach((item, index) => {
            if(item && index!==0){
                rows.push(
                    {
                        id: item[3],
                        name: item[4],
                        district: item[2],
                        cases: item[9],
                        risk: getRisk(item[9]) ,
                    }
                )
            }
        })
    }
    return rows
}

const getRisk = (cases) => {
    if (cases < 1) return <><CheckmarkFilled24 style={{fill: 'green'}}/></> 
    if (cases >= 1 && cases <5) return <><WarningAltFilled24 style={{fill: '#f0e51a'}}/></> 
    if (cases >=5 && cases <9) return <><WarningAltFilled24 style={{fill: 'orange'}}/></> 
    if (cases >=9) return <><WarningAltFilled24 style={{fill: 'red'}}/></> 
}

class AllSchoolsTable extends React.Component {
    state = {
        paginationPage: 1,
        paginationPageSize: 10,
      };

    getPaginationProps = () => {
        const {paginationPage, paginationPageSize } = this.state

        return {
            page: paginationPage,
            pageSize: paginationPageSize, 
            onChange: this.handlePaginationChange,
        }
    }

    getCurrentPageRows = rows => {
        let lastItemIndex = 0
        let {paginationPage} = this.state
        const {paginationPageSize } = this.state

        if (paginationPage === 1 || rows.length <= paginationPageSize) {
            lastItemIndex = paginationPageSize +1
            paginationPage = 1
        } else {
            lastItemIndex = (paginationPageSize * paginationPage) +1
        }
        return rows.slice(((paginationPage - 1) * paginationPageSize)+1, lastItemIndex)
    }

    handlePaginationChange = e => {
        this.setState({
            paginationPage: e.page,
            paginationPageSize: e.pageSize,
        })
    }
    
    render(){
        const renderProps = {
            getCurrentPageRows: this.getCurrentPageRows,
            getPaginationProps: this.getPaginationProps,
        }
        const row_data = getRows(this.props.data)
        return (
            <div className="modal-table-container">
                {row_data.length ===0 ?
                <Loading
                    description="Active loading indicator" withOverlay={false}
                /> :
                <DataTable rows={row_data} headers={headerData} onSelected={this.props.onSelect} setRows={this.props.setRows}>
                    {({ rows, headers, selectedRows, getHeaderProps, getRowProps, getSelectionProps, getTableProps, getToolbarProps, onInputChange }) => (
                        <TableContainer>
                            <TableToolbar {...getToolbarProps()}>
                                <TableToolbarContent>
                                    <TableToolbarSearch
                                        onChange={onInputChange}
                                    />
                                </TableToolbarContent>
                            </TableToolbar>
                        <Table {...getTableProps()}>
                            <TableHead>
                            <TableRow>
                                {headers.map((header) => (
                                <TableHeader {...getHeaderProps({ header })}>
                                    {header.header}
                                </TableHeader>
                                ))}
                            </TableRow>
                            </TableHead>
                            <TableBody>
                            {renderProps.getCurrentPageRows(rows).map((row) => (
                                <TableRow key={row.id}
                                    /*{...getRowProps({ row })}
                                    onClick={(evt) => {
                                       action('TableRow onClick')(evt);
                                    }}*/>
                                {row.cells.map((cell) => (
                                    <TableCell key={cell.id}>{cell.value}</TableCell>
                                ))}
                                </TableRow>
                            ))}
                            </TableBody>
                        </Table>
                        <Pagination
                            {...renderProps.getPaginationProps()}
                            pageSizes={[10, 20, 30, 50, 60, 75]}
                            itemsPerPageText = {'Items per page'}
                            backwardText={'Prev'}
                            forwardText={'Next'}
                            totalItems={rows.length}
                        />
                        </TableContainer>
                    )}
                </DataTable>
                }
            </div>
        );
    }
}

//MOCK DATA
const headerData = [
    {
      header: 'School Name',
      key: 'name',
    },
    {
      header: 'District',
      key: 'district',
    },
    {
      header: 'Cases',
      key: 'cases',
    },
    {
      header: 'Risk Level',
      key: 'risk',
    },
  ];

  export default AllSchoolsTable;