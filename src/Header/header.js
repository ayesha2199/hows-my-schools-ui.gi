import './style.scss'
import { ReactComponent as Icon } from '../school-icon.svg'
import {withRouter} from 'react-router-dom';
import { useHistory } from "react-router";

const Header = () => {
  const history = useHistory()
  return (
    <div className="header-container">
      <div className="tabs-container">
          <div className="tab" onClick={() => history.push({pathname:"/"})}>{"Home"}</div>
          <div className="tab" onClick={() => history.push({pathname:"/schools"})}>{"All Schools"}</div>
      </div>
      <div className="title-container">
          <div className="icon">
            <Icon/>
          </div>
          {"How's my School?"} 
        </div>
    </div>
  );
}

export default withRouter(Header);